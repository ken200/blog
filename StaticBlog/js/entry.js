/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="typings/marked/marked.d.ts" />
var blog;
(function (blog) {
    var entry;
    (function (entry) {
        var views;
        (function (views) {
            var EntryContainer;
            (function (EntryContainer) {
                /**
                 * ドキュメントタイトルの初期化
                 */
                function initDocumentTitle($docTitle, title) {
                    $docTitle.text(title);
                }
                /**
                 * ページタイトルの初期化
                 */
                function initHeader($header, title, tags) {
                    var HeaderTitle = function (title) {
                        $header.append("<h1>" + title + "</h1>");
                    };
                    var HeaderTagList = function (tags) {
                        tags.map(function (tag) {
                            $header.append($("<span>")
                                .addClass("label")
                                .addClass("label-primary")
                                .addClass("tag_label")
                                .css("margin-left", "5px")
                                .text(tag));
                        });
                    };
                    HeaderTitle(title);
                    HeaderTagList(tags);
                }
                /**
                 * エントリー本文の初期化
                 */
                function initBody($body, data) {
                    // 「*** [タイトル]・・・・・・・・ ***」のように記載すると BootstrpperのAlert表示するようにRendererを調整
                    var myRenderer = new marked.Renderer();
                    myRenderer.strong = function (text) {
                        var matches = text.match(/^<em>\s*\[(.+?)\]\s*(.+)\s*$/);
                        if (matches === null || matches.length !== 3)
                            return marked.Renderer.prototype.strong.call(myRenderer, text);
                        return '<div class="alert alert-danger" role="alert"><h3>' + matches[1] + '</h3><p>' + matches[2] + '</p></div>';
                    };
                    $body.append(marked(data, { renderer: myRenderer }));
                }
                /**
                 * フッターの初期化
                 */
                function initFooter($footer, prev, next) {
                    var $ele = $("<div class='row'>");
                    var createEntryLink = function (summary, titleHeadPrefix, titleTailPrefix) {
                        return summary && $("<a>")
                            .attr("href", "entry.html?id=" + summary.id)
                            .css("font-size", "large")
                            .text(titleHeadPrefix + summary.title + titleTailPrefix);
                    };
                    $("<div>").appendTo($ele)
                        .addClass("col-md-4")
                        .addClass("text-center")
                        .addClass("col-xs-4")
                        .append(createEntryLink(prev, "< ", ""));
                    $("<div>").appendTo($ele)
                        .addClass("col-md-4")
                        .addClass("text-center")
                        .addClass("col-xs-4")
                        .append("<a href='/blog' style='font-size: large;'>トップ</a>");
                    $("<div>").appendTo($ele)
                        .addClass("col-md-4")
                        .addClass("text-center")
                        .addClass("col-xs-4")
                        .append(createEntryLink(next, "", " >"));
                    $footer.append($ele);
                }
                function init(entryId) {
                    var findById = function (src, id) {
                        for (var i = 0; i < src.length; i++) {
                            if (src[i].id === id)
                                return i;
                        }
                        return -1;
                    };
                    var getSummary = function (src, idx, getNextIdx) {
                        if (idx < 0 || src.length - 1 < idx)
                            return null;
                        console.log("[getSummaryu] id:", src[idx].id, " status:", src[idx].status);
                        if (!blog.models.EntrySummary.isDraftEntry(src[idx]))
                            return src[idx];
                        return getSummary(src, getNextIdx(idx), getNextIdx);
                    };
                    var getSummaryNext = function (src, idx) {
                        return getSummary(src, idx, function (i) { return i + 1; });
                    };
                    var getSummaryPrev = function (src, idx) {
                        return getSummary(src, idx, function (i) { return i - 1; });
                    };
                    var success = function (list) {
                        //entryId未指定時はトップページへリダイレクトする。
                        if (!entryId) {
                            location.href = "index.html";
                            return;
                        }
                        var currentIdx = findById(list, entryId);
                        var current = list[currentIdx];
                        var prev = getSummaryPrev(list, currentIdx - 1);
                        var next = getSummaryNext(list, currentIdx + 1);
                        blog.models.EntryDocument.load(current.id).then(function (data) {
                            initDocumentTitle($("#document_title"), current.title);
                            initHeader($("#entry_header"), current.title, current.tags);
                            initBody($("#entry_container"), data);
                            initFooter($("#entry_footer"), prev, next);
                        });
                    };
                    var fail = function () { console.log("EntryContainer.init() error"); };
                    blog.models.EntrySummary.load($("#entry_list").attr("src")).then(success, fail);
                }
                EntryContainer.init = init;
            })(EntryContainer = views.EntryContainer || (views.EntryContainer = {}));
        })(views = entry.views || (entry.views = {}));
    })(entry = blog.entry || (blog.entry = {}));
})(blog || (blog = {}));
//main
$(document).ready(function () {
    var id = null;
    var param = location.search;
    if (param.indexOf("id=") >= 0) {
        id = param.split("id=")[1]; //todo: 要実装
    }
    blog.entry.views.EntryContainer.init(id);
});
