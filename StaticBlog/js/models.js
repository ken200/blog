/// <reference path="typings/jquery/jquery.d.ts" />
var blog;
(function (blog) {
    var models;
    (function (models) {
        var EntrySummary;
        (function (EntrySummary) {
            function load(url) {
                var d = $.Deferred();
                var success = function (data, textStatus, jqXHR) {
                    d.resolve(data);
                };
                var fail = function (jqXHR, textStatus, errorThrown) {
                    d.reject();
                };
                $.ajax({ url: url }).then(success, fail);
                return d.promise();
            }
            EntrySummary.load = load;
            function isDraftEntry(summary) {
                return summary.status && summary.status === "draft";
            }
            EntrySummary.isDraftEntry = isDraftEntry;
        })(EntrySummary = models.EntrySummary || (models.EntrySummary = {}));
        var EntryDocument;
        (function (EntryDocument) {
            function load(id) {
                var d = $.Deferred();
                var success = function (data, textStatus, jqXHR) {
                    d.resolve(data);
                };
                var fail = function (jqXHR, textStatus, errorThrown) {
                    d.reject();
                };
                $.ajax({ url: "md/" + id + ".md" }).then(success, fail);
                return d.promise();
            }
            EntryDocument.load = load;
        })(EntryDocument = models.EntryDocument || (models.EntryDocument = {}));
    })(models = blog.models || (blog.models = {}));
})(blog || (blog = {}));
