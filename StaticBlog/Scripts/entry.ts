﻿/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="typings/marked/marked.d.ts" />


interface RendererStatic {
    options: any;
    code(code, lang, escaped): string;
    blockquote(quote): string;
    html(html): string;
    heading(text, level, raw): string;
    hr(): string;
    list(body, ordered): string;
    listitem(text): string;
    paragraph(text): string;
    table(header, body): string;
    tablerow(content): string;
    tablecell(content, flags): string;
    strong(text: string): string;
    em(text): string;
    codespan(text): string;
    br(): string;
    del(text): string;
    link(href, title, text): string;
    image(href, title, text): string;
}

interface RendererFactory {
    new (): RendererStatic;
}

interface MarkedStatic {
    Renderer: RendererFactory;
}

module blog.entry.views {

    export module EntryContainer {

        /**
         * ドキュメントタイトルの初期化
         */
        function initDocumentTitle($docTitle: JQuery, title: string){
            $docTitle.text(title);
        }

        /**
         * ページタイトルの初期化
         */
        function initHeader($header: JQuery, title: string, tags: string[]){

            var HeaderTitle = (title: string) => {
                $header.append(`<h1>${title}</h1>`);
            };

            var HeaderTagList = (tags: string[]) => {
                tags.map((tag) => {
                    $header.append($("<span>")
                        .addClass("label")
                        .addClass("label-primary")
                        .addClass("tag_label")
                        .css("margin-left", "5px")
                        .text(tag))
                });
            };

            HeaderTitle(title);
            HeaderTagList(tags);
        }

        /**
         * エントリー本文の初期化
         */
        function initBody($body: JQuery, data: any) {

            // 「*** [タイトル]・・・・・・・・ ***」のように記載すると BootstrpperのAlert表示するようにRendererを調整
            var myRenderer = new marked.Renderer();
            myRenderer.strong = (text) => {
                var matches = text.match(/^<em>\s*\[(.+?)\]\s*(.+)\s*$/);
                if (matches === null || matches.length !== 3)
                    return marked.Renderer.prototype.strong.call(myRenderer, text);
                return '<div class="alert alert-danger" role="alert"><h3>' + matches[1] + '</h3><p>' + matches[2] + '</p></div>';
            };

            $body.append(marked(data, {renderer: myRenderer}));
        }

        /**
         * フッターの初期化
         */
        function initFooter($footer: JQuery, prev: blog.models.IEntrySummary, next: blog.models.IEntrySummary){

            var $ele = $("<div class='row'>");

            var createEntryLink = (summary: blog.models.IEntrySummary, titleHeadPrefix: string, titleTailPrefix: string) => {
                return summary && $("<a>")
                    .attr("href", "entry.html?id=" + summary.id)
                    .css("font-size", "large")
                    .text(titleHeadPrefix + summary.title + titleTailPrefix);
            }

            $("<div>").appendTo($ele)
                .addClass("col-md-4")
                .addClass("text-center")
                .addClass("col-xs-4")
                .append(createEntryLink(prev, "< ", ""));

            $("<div>").appendTo($ele)
                .addClass("col-md-4")
                .addClass("text-center")
                .addClass("col-xs-4")
                .append("<a href='/blog' style='font-size: large;'>トップ</a>");

            $("<div>").appendTo($ele)
                .addClass("col-md-4")
                //.addClass("col-md-offset-4")
                .addClass("text-center")
                .addClass("col-xs-4")
                //.addClass("col-xs-offset-4")
                .append(createEntryLink(next, "", " >"));

            $footer.append($ele);
        }

        export function init(entryId: string) {

            var findById = (src: models.IEntrySummary[], id: string) => {
                for (var i = 0; i < src.length; i++) {
                    if (src[i].id === id)
                        return i;
                }
                return -1;
            };

            var getSummary = (src: models.IEntrySummary[],idx: number,getNextIdx: (i: number) => number) => {
                if (idx < 0 || src.length - 1 < idx)
                    return null;

                console.log("[getSummaryu] id:", src[idx].id, " status:", src[idx].status);

                if (!blog.models.EntrySummary.isDraftEntry(src[idx]))
                    return src[idx];
                return getSummary(src, getNextIdx(idx), getNextIdx);
            };

            var getSummaryNext = (src: models.IEntrySummary[], idx: number) => {
                return getSummary(src, idx, (i) => { return i + 1; });
            };

            var getSummaryPrev = (src: models.IEntrySummary[], idx: number) => {
                return getSummary(src, idx, (i) => { return i - 1; });
            };

            var success = (list) => {
                
                //entryId未指定時はトップページへリダイレクトする。
                if (!entryId) {
                    location.href = "index.html";
                    return;
                }

                var currentIdx = findById(list, entryId);
                var current = list[currentIdx];
                var prev = getSummaryPrev(list, currentIdx - 1);
                var next = getSummaryNext(list, currentIdx + 1);

                models.EntryDocument.load(current.id).then((data) => {
                    initDocumentTitle($("#document_title"), current.title);
                    initHeader($("#entry_header"), current.title, current.tags);
                    initBody($("#entry_container"), data);
                    initFooter($("#entry_footer"), prev, next); 
                });
            };

            var fail = () => { console.log("EntryContainer.init() error"); };

            models.EntrySummary.load($("#entry_list").attr("src")).then(success, fail);
        }
    }
}

//main
$(document).ready(() => {
    var id = null;
    var param = location.search;
    if (param.indexOf("id=") >= 0) {
        id = param.split("id=")[1]; //todo: 要実装
    }
    blog.entry.views.EntryContainer.init(id);
});
