﻿/// <reference path="typings/jquery/jquery.d.ts" />

module blog.index.views {

    export module EntryListTable {

        export function init($root : JQuery, url: string) {

            var success = (list: blog.models.IEntrySummary[]) => {
                var len = list.length;
                var pinRowHtml = "";
                var rowHtml = "";
                for (var i = len - 1; i >= 0; i--) {
                    if (blog.models.EntrySummary.isDraftEntry(list[i]))
                        continue;
                    //$root.append(EntryListRow.createHtml(list[i]));
                    if (list[i].pined)
                        pinRowHtml += EntryListRow.createHtml(list[i]);
                    else
                        rowHtml += EntryListRow.createHtml(list[i]);
                }
                $root.append(pinRowHtml + rowHtml);
            };

            var fail = () => { console.log("EntryListTable.init() error"); };

            blog.models.EntrySummary.load(url).then(success, fail);
        }
    }

    module EntryListRow {
        export function createHtml(rowData: blog.models.IEntrySummary) {
            var pin = rowData.pined ? "<span class='glyphicon glyphicon-pushpin pin_logo'></span>" : "";
            var url = "/blog/entry.html?id=" + rowData.id;
            var title = rowData.title;
            var wroteAt = rowData.wrote_at.year + "/" + rowData.wrote_at.month + "/" + rowData.wrote_at.day;

            var tags = rowData.tags.reduce((prev, current, idx, src) => {
                return prev + `<span class='label label-primary tab_label' style='margin-left:5px;'>${current}</span>`;
            }, "");

            return `<tr><td>${pin}</td><td><a href=${url}>${title}</a></td><td class='hidden-xs'>${tags}</td><td>${wroteAt}</td></tr>`;
        }
    }
}

//main
$(document).ready(() => {
    blog.index.views.EntryListTable.init($("#entry_list_table"), $("#entry_list_src").attr("src"));
});