﻿/// <reference path="typings/jquery/jquery.d.ts" />

module blog.models {

    export interface IEntrySummary {
        id: string;
        title: string;
        tags: Array<string>;
        wrote_at: { year: number; month: number; day: number };
        pined: boolean;
        status: string;
    }

    export module EntrySummary {

        export function load(url: string){
            var d = $.Deferred<IEntrySummary[]>();

            var success = (data: any, textStatus: string, jqXHR: JQueryXHR) => {
                d.resolve(data);
            };

            var fail = (jqXHR: JQueryXHR, textStatus: string, errorThrown: any) => {
                d.reject();
            };

            $.ajax({ url: url }).then(success, fail);

            return d.promise();
        }

        export function isDraftEntry(summary: IEntrySummary) {
            return summary.status && summary.status === "draft";
        }
    }

    export interface IEntryDocument { }

    export module EntryDocument {
        export function load(id: string) {
            var d = $.Deferred<IEntryDocument>();

            var success = (data: any, textStatus: string, jqXHR: JQueryXHR) => {
                d.resolve(data);
            };

            var fail = (jqXHR: JQueryXHR, textStatus: string, errorThrown: any) => {
                d.reject();
            };

            $.ajax({ url: "md/" + id + ".md" }).then(success, fail);

            return d.promise();
        }
    }
}