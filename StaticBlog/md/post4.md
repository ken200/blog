﻿http://hokaccha.github.io/slides/javascript_design_and_test/

## それぞれの役割

+ Model : データの保持と操作。
+ View : DOMを操作する。

## Modelのコード例

参照先スライドから引用。

***Todoのインスタンス(リスト)をTodo自身に持たせる***ことで、データの保持を実現している。これはJavaScript独特な持ち方と思われる。

Viewとの連携はイベント通知を用いて疎結合な設計にしている。
例ではBackbone.js使ってるけど、本来はPubSub.js使うとか自作すべき。

```model.todo.js
function Todo(data){
	this.text = data.text;
	this.complete = !!data.complete;
}

//説明簡略化の為Backbone.jsの機能を使ってイベント関係の機能を付け加える。
$.extend(Todo.prototype, Events);
$.extend(Todo, Events);

Todo.prototype.setComplete = function(complete){
	this.complete = !!complete;
	this.trigger("change:complete", this);
};

//自身のインスタンスを保持する配列
Todo.list = [];

//新規Todoを追加するためのクラスメソッド
Todo.add = function(text){
	var todo = new Todo({text: text});
	Todo.list.push(todo);
	this.trigger("add", todo);
};
```

## Viewのコード例

参照先スライドから引用。

コンポ―ネント単位でViewを作成している。(TodoListコンテナ、TodoListアイテム)

モデル状態の影響を受ける箇所をViewクラスで表わす。

この時点でスパゲッティ臭が。。ViewはDSLで宣言的に記述すべきものと思った。

```model.todo.js
//Todo一覧Viewクラス
function TodoListView($el){
	this.$el = $el;
	Todo.on("add", this.add.bind(this));
}

TodoListView.prototype.add = function(todo){
	var item = new TodoListItemView(todo);
	this.$el.append(item.$el);
};

//Todo一覧の要素を管理するViewクラス
function TodoListItemView(todo){
	this.todo = todo;
	this.$el = $('<li><input type="checkbox">' + todo.text + '</li>');
	this.$checkbox = this.$el.find('input[type="checkbox"]');
	
	this.$checkbox.change(this.onchangeCheckbox.bind(this));
	this.todo.on("change:complete", this.onchangeComplete.bind(this));
}

//チェックボックスの値が変わった時のイベントハンドラ
TodoListItemView.prototype.onchangeCheckbox = function(){
	this.todo.setComplete(this.$checkbox.is("is:checked"));
};

//モデルのcompleteプロパティの値が変わった時のイベントハンドラ
TodoListItemView.prototype.onchangeCheckbox = function(){
	if(this.todo.complete){
		this.$el.addClass("complete");
	}
	else{
		this.$el.removeClass("complete");
	}
	
	this.$checkbox.attr("checked", this.todo.complete);
};
```

## Controllerのコード例

スライド中では言及なかったが、main.jsがコントローラーに相当する。

```main.js
$(function(){
	new TodoFormView($(".todoForm"));
	new TodoListView($(".todoList"));
})
```

## htmlファイル

http://hokaccha.github.io/slides/javascript_design_and_test/demo/todo_4/index.html

```index.html

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>Sample</title>
<style>
body {
  font-size: 24px;
}

input {
  font-size: 20px;
}

.complete {
  text-decoration: line-through;
  color: gray;
}

.usualList {
  padding: 0;
  margin: 10px 0;
}
.usualList li {
  list-style: none;
  display: inline-block;
  background: #333;
  font-size: 16px;
  color: #FFF;
  padding: 5px 10px;
  border-radius: 10px;
  cursor: pointer;
}
.usualList li:hover {
  background: #666;
}
</style>
</head>
<body>

<form class="todoForm">
  <input type="text">
  <input type="submit">
</form>

<ul class="usualList">
  <li>歯磨きする</li>
  <li>ごはん食べる</li>
  <li>原稿を書く</li>
</ul>

<p><button class="completeAll">全て完了にする</button></p>

<ul class="todoList">
</ul>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
<script src="event.js"></script>
<script src="model.todo.js"></script>
<script src="view.todoForm.js"></script>
<script src="view.todoList.js"></script>
<script src="main.js"></script>
</body>
</html>
```
