﻿## ステートレス認証とは

WebAPIリクエスト時にAPIKeyなどのトークン情報を送信し、それを使ってアクセス可否を判断する認証方法。
リクエスト毎に認証を行うため"ステートレス"な認証である。

## 使用パッケージ

```
Nancy.Authentication.Stateless
```

## サンプルコード

https://github.com/NancyFx/Nancy/wiki/Stateless-Authentication
https://github.com/jchannon/Nancy.Demo.StatelessAuth

上記ページには、アプリケーション単位で認証有効設定するためのコード例が載っている。下記コードはモジュール単位で設定しているため若干異なる。また、キー情報の取得をクエリ文字列ではなくHeaderから取得するようにも変更している。

ユーザー情報取得周り(IUserApiMapperなど)はアプリケーション毎に作りこむ必要がある。Nancyが提供しているものではない。

```ApiModule.cs
using Nancy;
using Nancy.Authentication;
using Nancy.Authentication.Stateless;
using Nancy.Security;
using Nancy.Responses;

public class ApiModule : NancyModule
{
    public ApiModule(IUserApiMapper userValidator)
        : base("/api")
    {
        StatelessAuthentication.Enable(this, new StatelessAuthenticationConfiguration((ctx) =>
        {
            var keyName = "apikey";

            if (!ctx.Request.Headers.Keys.Any((k) => k == keyName))
            {
                return null;
            }

            return userValidator.GetUserFromAccessToken(ctx.Request.Headers[keyName].First());
        }));

        this.RequiresAuthentication();

        Get["hello"] = _ =>
        {
            return new JsonResponse(new { Message = "hello at " + DateTime.Now.ToString() }, new DefaultJsonSerializer());
        };
    }
}

public interface IUserApiMapper
{
    IUserIdentity GetUserFromAccessToken(string accessToken);
}

public class UserApiMapper : IUserApiMapper
{
    public IUserIdentity GetUserFromAccessToken(string accessToken)
    {
        if (accessToken == "fred")
            return new DemoUserIdentity { UserName = "Fred" };

        return null;
    }
}

public class DemoUserIdentity : IUserIdentity
{
    public string UserName { get; set; }

    public IEnumerable<string> Claims { get; set; }
}
```
