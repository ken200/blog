﻿文法はJavaScriptだけど正規表現はどの言語でも同じだと思われる。

## マッチ有無の確認

RegExpのtest()を使う。

```regex_example.js
var reg = new RegExp("^test.*$");
console.log(reg.test("testhoge"));  //true
console.log(reg.test("hogetest"));  //false
```

または //リテラルを使うことも可能。
パフォーマンス的にもこちらのほうがよい？

```regex_example2.js
console.log(/^test.*$/.test("testhoge"));  //true
console.log(/^test.*$/.test("hogetest"));  //false
```

動的に正規表現を変えたい場合はRegExpオブジェクトを使うことになる。
RegExpコンストラクタで文字列として正規表現を渡すため、プログラムでの組み立てが可能なため。

## 最短マッチ

デフォルトでは最大マッチだた、繰り返し指定子末尾に?をつけることで最短マッチするようになる。

```default_match.js
var str = "hoge_foo_bar_hoge_foo_bar_";
console.log(str.match(/hoge.+/)[0])  //"hoge_foo_bar_hoge_foo_bar_"
```

```max_range_match.js
var str = "hoge_foo_bar_hoge_foo_bar_";
console.log(str.match(/hoge.+?/)[0])  //"hoge_"
```


## グループにマッチした文字列の取得

マッチした値の取得と同様にstringのmatch()を使う。
マッチした全体文字列はIndex0に、それ以降にグループ毎文字列が格納されている。

```js
console.log("<hoge>ほげげ</hoge>".match(/<(\w+)>(.+)<\/\1>/)[0]); //"<hoge>ほげげ</hoge>"
console.log("<hoge>ほげげ</hoge>".match(/<(\w+)>(.+)<\/\1>/)[1]); //"hoge"
console.log("<hoge>ほげげ</hoge>".match(/<(\w+)>(.+)<\/\1>/)[2]); //"ほげげ"

```
