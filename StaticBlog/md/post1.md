﻿コード中のPostMessage#add() で要認証なAPIへのm.request()している。

```PostMessage.ts
module app.models.bbs {

    /**
     * 書き込みメッセージ
     */
    export class PostMessage {

        title: MithrilModelProperty<string>;
        userName: MithrilModelProperty<string>;
        content: MithrilModelProperty<string>;
        postDate: MithrilModelProperty<number>;

        constructor(data: { title: string; userName: string; content: string; postDate: number }) {

            var allow = (value: any) => { return true };
            var required = (value: any) => { return !!value };

            this.title = mm.propWithValidator(m.prop(data.title), allow);
            this.userName = mm.propWithValidator(m.prop(data.userName), required);
            this.content = mm.propWithValidator(m.prop(data.content), required);
            this.postDate = mm.propWithValidator(m.prop(data.postDate), allow);
        }

        private static createKeyParam(category: string, thread: string) {
            return { category: category, thread : thread };
        }

        static get(category : string, thread : string): MithrilPromise<Array<PostMessage>> {
            var url = "/api/message";
            var key = PostMessage.createKeyParam(category, thread);
            return m.request({ method: "GET", url: url, type: (low: any): PostMessage => { return new PostMessage(low); }, data : key});
        }

        static add(category : string, thread : string, postMsg: PostMessage): MithrilPromise<any> {
            var url = "/api/message";
            var addData = _.extend(postMsg, PostMessage.createKeyParam(category, thread));
            var config = (xhr, options) => { xhr.setRequestHeader("token", sys.token()); return xhr; };
            var extract = (xhr: XMLHttpRequest, options: MithrilXHROptions) => {
                var defaultExtract = (xhr) => { return xhr.responseText.length === 0 ? null : xhr.responseText; }
                var unauth = (xhr) => { return JSON.stringify({ status: xhr.status }); };
                return xhr.status = 401 ? unauth(xhr) : defaultExtract(xhr);
            };
            return m.request({ method: "POST", url: url, data: addData, config: config, extract: extract});
        }
    }
}
```

## m.request()のconfigパラメーター

XMLHttpRequestオブジェクト直触りしてリクエストヘッダーに値設定する関数を指定。

## m.request()のextractパラメーター

レスポンスの生データを使ってクライアントへ返す値を生成する関数を指定。
独自に生成する場合は、JSON文字列を返すこと。(JSON.stringify()の結果を返す。)

